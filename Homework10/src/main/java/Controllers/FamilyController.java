package Controllers;

import Entitiy.Family;
import Entitiy.Human;
import Entitiy.Pet;
import Services.FamilyService;


import java.util.List;


public class FamilyController {
    FamilyService familyService ;

    public FamilyController() {
        familyService = new FamilyService();
    }

    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
       familyService.displayAllFamilies();
    }
    public void getFamiliesBiggerThan(int number){
       familyService.getFamiliesBiggerThan(number);

    }
    public void getFamiliesLessThan(int number){
      familyService.getFamiliesLessThan(number);

    }
    public int countFamiliesWithMemberNumber(int number){
     return familyService.countFamiliesWithMemberNumber(number);
    }

    public  boolean createNewFamily(Human mother, Human father){
        return  familyService.createNewFamily(mother, father);

    }
    public boolean deleteFamilyByIndex(int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Family adoptChild(Family family, Human child){
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int number) {
      familyService.deleteAllChildrenOlderThen(number);
    }

    public int count(){
        return familyService.count();
    }
    public  Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public List<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet){
        familyService.addPet(index, pet);
    }



    public Family bornChild(Family family, String feminine, String masculine){

        return  familyService.bornChild(family, feminine, masculine);

    }

public  boolean saveFamily(Family family){
        return  familyService.addFamily(family);
}


}

import Controllers.FamilyController;
import Entitiy.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        Family family = new Family(new Human("Lili", "AAA", LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""),(new Human("Ali", "AAA", LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+"")));
        familyController.saveFamily(family);
        familyController.bornChild(family, "", "Orkhan");
        familyController.adoptChild(family, new Human("AA", "BBB", LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))+""));
        familyController.addPet(0, new Fish( "fish"));
        List<Family> allFamilies = familyController.getAllFamilies();
        familyController.displayAllFamilies();
        System.out.println(familyController.count());

    }
}

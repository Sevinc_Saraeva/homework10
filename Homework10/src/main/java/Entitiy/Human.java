package Entitiy;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TimeZone;


public class Human {
    private String name;
    private String surname;
    private long birthdate;
    private int iq_level;
    private Family family;
    private Map <String, String> shedule;

    public Human(String name, String surname, String birthdate) {
        this.name = name;
        this.surname = surname;
        this.birthdate = toEpochMilli(birthdate);
    }
    public Human(String name, String surname, String birthdate, int iq) {
        this(name, surname, birthdate);
        this.iq_level = iq;
    }
    public Human(String name, String surname, String birthdate, Family family) {
        this(name, surname, birthdate);
        this.family = family;
    }
    public Human(String name, String surname, String birthdate, int iq_level, Map<String, String> shedule) {
      this(name, surname, birthdate, iq_level);
        this.shedule = shedule;
    }
    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(long birthdate) {
        this.birthdate = birthdate;
    }

    public int getIq_level() {
        return iq_level;
    }

    public void setIq_level(int iq_level) {
        this.iq_level = iq_level;
    }

    public void greetPet() {
        System.out.println("hello, " + getFamily().getPet().get(0).getNickname());
    }
    public Map<String, String> getShedule() {
        return shedule;
    }

    public void setShedule(String[][] shedule) {
        shedule = shedule;
    }

    public long toEpochMilli(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        long l = 0;
        try {
            l = LocalDate.parse(birthDate, formatter).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return l;
    }

    public String describeAge() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(birthdate), TimeZone
                .getDefault().toZoneId());
        LocalDateTime now = LocalDateTime.now();
        String birth = formatter.format(dateTime);
        String year = birth.split("/")[2];
        String month = birth.split("/")[1];
        String day = birth.split("/")[1];
        return "day: " + day + ", month: "+ month+", year: "+year;
    }
    public String toStringFormatted(long l) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dateTime = Instant.ofEpochMilli(l).atZone(ZoneId.systemDefault()).toLocalDate();
        return formatter.format(dateTime);
    }



    @Override
    public String toString() {
        String sh = "";
        if (shedule != null) {
            for (Map.Entry<String, String> entry : shedule.entrySet()) {
                sh += "[" + entry.getKey() + ", " + entry.getValue() + "]";
            }
        }
        String s = "Human{name='" + name + "', surname='" + surname + "', birth date=" + describeAge()+ ", iq=" + iq_level + ", schedule=[" + sh + "]}";
        return s;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Human))
            return false;
        if (obj == this)
            return true;
        return this.getName() == ((Human) obj).getName() && this.getSurname() == ((Human) obj).getSurname();
    }
}
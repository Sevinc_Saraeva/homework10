package Entitiy;

public class DomesticCat extends Pet {
    public DomesticCat(String nickname ) {
        super(nickname);
        super.setSpecies(Species.DOMESTIC_CAT);
    }
    @Override
    public void respond() {
        System.out.println("Hello, owner. I am DomesticCat" + super.getNickname() + ". I miss you! ");
    }
}
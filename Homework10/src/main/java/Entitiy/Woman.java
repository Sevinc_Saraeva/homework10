package Entitiy;

public class Woman extends Human {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, String  year) {
        super(name, surname, year);
    }
    public Woman(String name, String surname, String birthdate, int iq) {
        super(name, surname, birthdate, iq);
    }
    public Woman(String name, String surname, String year, Family family) {
        super(name, surname, year, family);
    }
    public void makeup(){
        System.out.println(super.getName() + " is doing make up");
    }

    @Override
    public void greetPet() {
        System.out.println("hello, " + super.getFamily().getPet().get(0).getNickname());
    }
}

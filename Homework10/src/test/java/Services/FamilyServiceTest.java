package Services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import Entitiy.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    FamilyService familyService;
    Woman mother;
    Man father;
    Woman mother1;
    Man father2;
    Man child;
    Woman child1;
    Family family;
   Cat cat;

    @BeforeEach
    void  init(){
        familyService = new FamilyService();
         mother  = new Woman("Mother1", "AAA", "23/10/1970");
         father  = new Man("Mother1", "AAA", "23/10/1970");
         child = new Man("Child1", "AAA", "10/10/2000");
         family = new Family(mother, father);
         familyService.addFamily(family);
    }
    @Test
    void getAllFamilies() {
      assertEquals(1, familyService.getAllFamilies().size());
    }


    @Test
    void countFamiliesWithMemberNumber() {
        child = new Man("Child1", "AAA", "10/10/2000");
        child1 = new Woman("Child1", "AAA", "10/10/2000");
        familyService.getAllFamilies().get(0).addChild(child);
        familyService.getAllFamilies().get(0).addChild(child1);
        assertEquals(1,familyService.countFamiliesWithMemberNumber(4));
    }

    @Test
    void createNewFamily() {
        mother1 = new Woman("Mother1", "BBB", "03/05/1975");
        father2 = new Man("Father2", "BBB","03/05/1975" );
        assertTrue(familyService.createNewFamily(mother1, father2));

    }

    @Test
    void deleteFamilyByIndex() {
        assertTrue(familyService.deleteFamilyByIndex(0));
    }


    @Test
    void count() {
        assertEquals(1, familyService.count());
    }

    @Test
    void getFamilyById() {
        assertEquals(family, familyService.getFamilyById(0));
    }


    @Test
    void addPet() {
        cat = new Cat("cat");
        assertTrue(familyService.addPet(0, cat));
    }

    @Test
    void bornChild() {
        familyService.bornChild(family, "", "Eli");
        assertEquals(1, familyService.getAllFamilies().get(0).getChildren().size());
    }

    @Test
    void addFamily() {
        assertEquals(1, familyService.getAllFamilies().size());

    }
}